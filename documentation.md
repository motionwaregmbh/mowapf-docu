
# Einleitung




Dieses Dokument beschreibt das Benutzeroberfläche (GUI) der MoWa Box, genannt MoWa Cockpit. Es werden alle Komponenten der GUI vorgestellt und die gängigsten Anwendungsbeispiele beschrieben.
Die MoWa  Box ist eine Appliance, welche die Anbindung zentraler Services realisiert. Durch Einbringen der Box in die lokale Netzwerkinfrastruktur des Partnerbetriebes und unter Verwendung des
vorhandenen Internetzu-gangs kann eine transparente sowie sichere Kommunikation zwischen den vorgesehenen lokalen Applikationen (z.B. DMS oder DMS-BB) und den benötigten zentralen Diensten
(z.B. Superservice Triage) hergestellt werden. Das MoWa Cockpit ist die BenutzeroberflÃ¤che (GUI) der MoWa PF Box. Mittels dieser GUI können sie Einstellungen an Ihrer MoWa PF vornehmen und
Angaben zu dessen Status erhalten.

# Hardware Hersteller

Es existieren zwei Hardware Ausführungen der MoWa Box. In diesem Abschnitt werden die beiden Varianten nähre beschrieben.


| Dell                                                          | HP                                                               |
|---------------------------------------------------------------|------------------------------------------------------------------|
| ![DELL OptiPlex 3080](./img/dell.jpg){width=320 height=240}   | ![HP 800 G4 DM](./img/hp.jpg){width=320 height=240}              |
| DELL OptiPlex 3080                                            | HP 800 G4 DM                                                     |
| Intel Core i5-10400T Prozessor (bis zu 3,80 GHz), Hexa-Core   | Intel Core i5-8500 3.00GHz                                       |
| 8 GB RAM                                                      | 16 GB RAM                                                        |
| 256 GB SSD                                                    | 2 TB HDD                                                         |


# Inbetriebnahme der MoWa PF Box

## Voraussetzungen

Folgende Ports müssen für den Betrieb der MoWa Box freigeschaltet werden:

* 443 OUT zum Amazon Web Service (AWS)
* 443 OUT zum Synaptic-Panel
* 8780 IN
* 8788 IN 

## Auspacken und Inhalt überprüfen

Entnehmen sie das Gerät aus der Verpackung und vergewissern Sie sich, dass keine Beschädigungen vorhanden sind. Überprüfen sie den Inhalt des Pakets auf Vollständigkeit. 

Im Lieferumfang enthalten sind folgenden Komponenten:

* MoWa PF Box
* Spannungsversorgung (Netzteil 19,5V 3,34A (DELL) oder 4,62A(HP))
* Lieferschein
* Passwortbrief

Stellen sie das Gerät an einem geeigneten Ort auf.

## Anschließen

Schließen sie das Gerät an ihr lokales Netzwerk an (Netzwerkkabel ist nicht im Lieferumfang enthalten).
Verbinden sie anschließend das Gerät mit dem Stromnetz mittels des mitgelieferten Netzteils. Verwenden sie ausschließlich das beiliegende Netzteil. 

## Einschalten

Betätigen sie den Power-Button, um das Gerät einzuschalten. Nachdem die MoWa PF Box hochgefahren ist und eine aktive Netzwerkverbindung besteht, können Sie das MoWa Cockpit im Browser unter folgender URL aufru-fen:

`http://xxxxxx:8080`

## Support Kontaktieren

Für die Ersteinrichtung ist es erforderlich den Support zu kontaktieren. Bitte senden sie eine Mail an die unten angegebene Supportadresse mit der Bitte um Ersteinrichtung.
Geben sie in der Mail bitte die Nummer ihrer MoWa PF Box an. Diese finden sie auf der Box selbst im Lieferschein oder im Passwortbrief. Sollten sie Beschädigungen am Gerät feststellen oder andere Probleme bei der Inbetriebnahme auftreten, 
wendden sie sich bitte umgehend an den Support unter:

[dmsgw@synaptic-systems.com](mailto:dmsgw@synaptic-systems.com)

# MoWa Cockpit

Dieses Kapitel beschreibt das MoWa Cockpit im Allgemeinen. Es wird eine grobe Übersicht über alle Darstellun-gen und Auswahlmöglichkeiten gegeben. Konkrete Anwendungsbeispiele befinden sich in Kapitel 4.

## Allgemeiner Aufbau

Dieser Abschnitt beschreibt den allgemeinen Aufbau des MoWa Cockpits.

![Übersicht](./img/uebersicht.png){width=320 height=240}

1. Hostname
Dies ist der Hostname ihrer MoWa PF Box. Sie können den Hostname unter „Konfiguration“ anpassen.
2. Auswahl
Hier finden Sie die einzelnen Funktionen und Ansichten, die Ihnen das MoWa Cockpit zur Verfügung stellt.
3. Anzeigebereich
Im Anzeigebereich werden die Details der jeweiligen Ansicht dargestellt.
4. Einstellungen
In diesem Bereich können Sie die Einstellungen Ihres MoWa PF Users anpassen, wie z.B. das Ändern des Pass-wortes.

## Ueberblick

Dies ist die Startseite des MoWa Cockpit. In dieser Ansicht erhalten Sie einen Überblick über den Status der MoWa Box.

Die dargestellten Elemente sind:

* Meldungen
* Verbrauch
* System Information
* Konfiguration

![Ueberblick](./img/Ueberblick.png)

### Meldungen

In diesem Fenster werden Statusmeldungen angezeigt.

### Verbrauch

Dieses Fenster zeigt die aktuelle Auslastung der MoWa Box an. Für detailliertere Angaben kann auf „Diagramme anzeigen“ geklickt werden.

#### Diagramme

![Diagramme](./img/Diagramme.png "Ubersicht Diagramme")

### System Information

In diesem Fenster werden Informationen über die Hardware der MoWa Box bereitgestellt. Einen detaillierteren Überblick erhält man, wenn man „Hardware-Details anzeigen“ klickt.

#### Hardware Details

In dieser Ansicht werden Informationen über die Hardware der MoWa Box detailliert aufgelistet.

![Hardware Details](./img/hardware%20details.png)

### Konfiguration

In der Konfiguration können diverse Einstellungen am Gerät durchgeführt werden. Diese umfassen unter anderem das Ändern des Hostnamens und das Anpassen der Systemzeit.

## Protokolle

Im Bereich Protokolle werden die Logs angezeigt. Diverse Einstellungsmöglichkeiten am oberen Rand des Fensters ermöglichen eine gezielte Suche nach bestimmten Ereignissen.

![Protokolle](./img/protokolle2.png)

### Speicher

Der Bereich Speicher gibt unter anderem eine Übersicht über die Auslastung der Festplatten und des verfügba-ren Speicherplatzes.

![Speicher](./img/speicher.png)

* Devices: Keine Berechtigung Speicher zu verwalten
* Dateisystem -> Ansicht -> Keine Rechte Partitionstabellen anzulegen

### Netzwerk

Dieser Abschnitt beschreibt die Netzwerkeinstellungen. Unter anderem können die Netzwerkeinstellungen in diesem Abschnitt angepasst werden. Darunter fällt z.B. das manuelle Vergeben einer IP Adresse.

![Netzwerk](./img/netzwerk.png)

### Container

In der Container Ansicht werden alle vorhandenen Container aufgelistet. Außerdem kann man hier einzelne Container neustarten, löschen oder anhalten.

![Container](./img/container.png)

Die Synaptic Dienste laufen als Dokcer Container.

// TODO 
// Bitte hier kurz auf die Synaptic Dienste eingehen, dass diese als Docker laufen

### Dienste

Hier befindet sich eine Übersicht aller laufenden Dienste. Die Ansicht kann nach diversen Einstellungen gefiltert werden, um gezielt nach bestimmten Diensten zu suchen. Außerdem kann man sich in den Reitern oben im Fenster noch die „Ziele“, „Sockets“, "Timer" und "Pfade" anzeigen lassen. Die Namen der Dienste werden ange-zeigt inklusive Beschreibung des Dienstes und Status. Ein Klick auf den jeweiligen Dienst zeigt weitere Informati-onen an.

![Dienste](./img/dienste.png)

### Synaptic Logs

In dieser Ansicht werden alle Logs, die Synaptic betreffen, angezeigt. Die Ansicht kann mit den Filtern oben im Fenster angepasst werden.

![Synaptic Logs](./img/synaptic logs.png)

# Anwendungsbeispiele

Der folgende Abschnitt beschreibt die gängigsten Funktionen, die der User ausführen kann.

## Netzwerkeinstellungen ändern

Um die Netzwerkeinstellungen zu ändern, wird wie folgt vorgegangen:

![Netzwerkeinstellungen 1](./img/Netzwerkeinstellungen/1.png)

Zuerst wird der „Netzwerk“ Reiter ausgewählt.
Dann wird die passende Netzwerkkarte ausgewählt: - enp1s0

![Netzwerkeinstellungen 2](./img/Netzwerkeinstellungen/2.png)

In Interface-Ansicht wird IP4 ausgewählt

![Netzwerkeinstellungen 3](./img/Netzwerkeinstellungen/3.png)

In diesem Fenster können nun die Einstellungen nach den eigenen Bedürfnissen angepasst werden.

### Manuelle Netzwerkeinstellungen

Sollte es nötig sein die Netzwerkeinstellungen von DHCP auf Manuell zu stellen, kann man, wie in der folgenden Abbildung gezeigt, die Einstellungen anpassen.

![Netzwerkeinstellungen 4](./img/Netzwerkeinstellungen/4.png)

#### Manuelle IP-Adressen

In der Zeile „Adresse“ wird die Option „Manuell“ ausgewählt. In den Feldern können dann die Adresse, Subnetz-maske und Gateway eingetragen werden.

#### DNS

Soll auch der DNS-Server manuell eingetragen werden, wird zunächst die Funktion „Automatisch“ deaktiviert. Dann kann über das + Zeichen der gewünschte DNS-Server manuell hinzugefügt werden.

## Container Restart

Um einen Container neuzustarten, wird wie folgt vorgegangen:

![Container Restart 1](./img/containerrestart1.png)

In der Hauptansicht wird auf „Container“ geklickt. Anschließend öffnet sich eine Ansicht, in der die einzelnen Container aufgelistet sind.

![Container Restart 2](./img/containerrestart2.png)

Um den Container neuzustarten, klickt man erst auf den entsprechenden Container.

![Container Restart 3](./img/containerrestart3.png)

Anschließend klickt man in der sich öffnenden Ansicht auf „Neustarten“. Der Container wird anschließend ohne Bestätigung neugestartet.

## Anpassen der Systemzeit

// TODO ntp einrichten

Um die Systemzeit anzupassen, wird wie folgt vorgegangen:

![Systemzeit 1](./img/Systemzeit1.png)

In der Hauptansicht „Überblick“ wird im Fenster „Konfiguration“ auf die Auswahl „Systemzeit“ geklickt. 

![Systemzeit 2](./img/Systemzeit2.png)

In dem sich daraufhin darauf öffnenden Fenster, kann man die Zeitzone festlegen und die Systemzeit entweder Manuell einstellen oder einen NTP Server eintragen.

## Logs einsehen

Um die Logs einzusehen, wird die Ansicht „Protokolle“ aufgerufen. Hier können nun die Logs eingesehen und gefiltert werden.

![Logs einsehen](./img/logs.png)

## Synaptic logs einsehen

Durch Aufrufen der Ansicht „Synaptic Logs“ können die Synaptic Logs eingesehen und gefiltert werden.

![Synaptic Logs einsehen](./img/synapticlogsbeispiel.png)

## Passwort ändern

Aktuell nicht möglich.

## Neustart/Herunterfahren

Um einen Neustart der MoWa Box durchzuführen, oder um diese herunterzufahren, wird wie folgt vorgegangen:

![Neustart 1](./img/neustart1.png)

In der Ansicht „Überblick“ befindet sich oben rechts im Fenster die Option „Neustarten“. Wenn man diese an-klickt, öffnet sich das Auswahlfenster und man bekommt die Möglichkeit das Gerät neuzustarten oder herunterzufahren.

![Neustart 2](./img/neustart2.png)

# Hostname ändern

Um den Hostname zu ändern klickt man in der Hauptansicht „Überblick“ im Fenster „Konfiguration“ auf Hostname (bearbeiten).

![Hostname ändern 1](./img/hostname.png)

Hier kann man nun sowohl den Anzeigenamen und den echten Namen ändern.

![Hostname ändern 2](./img/hostname2.png)

Für den echten Rechnernamen gilt:

Ein echter Hostname darf nur Kleinbuchstaben, Ziffern, Bindestriche und Punkte enthalten (mit ausgefüllten Unterdomänen).

# Anhang

## Dell Beep Codes (Optiplex)

Im Fehlerfall kann es vorkommen, dass das Gerät nicht hochfahren kann. In diesem Fall sendet die MoWa Box Piepton Codes aus, ähnlich wie Morse Zeichen.
Diese Pieptöne werden in diesem Abschnitt näher beschrieben.

1 – Ein Piepton
2 – Zwei Pieptöne
3 – Drei Pieptöne
4 – Vier Pieptöne

```markdown
Beispiel: 1-2-2 Ein Piepton, gefolgt von 2 Pieptönen, gefolgt von 2 weiteren Pieptönen.
```

Signaltoncodes für die Diagnose

| **Code**                            | **Ursache** |
| ----------------------------------- |-------------|
| 1-1-2                               | Mikroprozessor-Registerausfall |
| 1-1-3                               | NVRAM |
| 1-1-4                               | ROM-BIOS-Prüfsummenfehler |
| 1-2-1                               | Programmierbarer Intervall-Timer |
| 1-2-2                               | DMA-Initialisierungsfehler |
| 1-2-3                               | Lese-/Schreibfehler des DMA-Seitenregisters |
| 1-3-1 bis 2-4-4                     | DIMMs werden nicht ordnungsgemäß erkannt oder verwendet |
| 3-1-1                               | Fehler des Slave-DMA-Registers |
| 3-1-2                               | Fehler des Master-DMA-Registers |
| 3-1-3                               | Fehler des Master-Interruptmaskenregisters |
| 3-1-4                               | Fehler des Slave-Interruptmaskenregisters |
| 3-2-2                               | Ladefehler des Interruptvektors |
| 3-2-4                               | Fehler beim Testen des Tastatur-Controllers |
| 3-3-1                               | Unterbrechung der NVRAM-Stromversorgung |
| 3-3-2                               | NVRAM-Konfiguration |
| 3-3-4                               | Fehler beim Video-Speichertest |
| 3-4-1                               | Fehler bei der Bildschirminitialisierung |
| 3-4-2                               | Fehler beim Bildschirmrücklauf |
| 3-4-3                               | Fehler bei der Suche nach dem Video-ROM |
| 4-2-1                               | Kein Zeitsignal |
| 4-2-2                               | Shutdownfehler |
| 4-2-3                               | Gate A20-Fehler |
| 4-2-4                               | Unerwarteter Interrupt im geschützten Modus |
| 4-3-1                               | Speicherfehler oberhalb der Adresse 0FFFFh |
| 4-3-3                               | Fehler des Zeitgeber-Chipzählers 2 |
| 4-3-4                               | Uhrzeit funktioniert nicht |
| 4-4-1                               | Fehler beim Testen der seriellen oder parallelen Schnittstelle |
| 4-4-2                               | Fehler beim Dekomprimieren des Codes im Shadow-RAM |
| 4-4-3                               | Fehler beim Testen des mathematischen Coprozessors |
| 4-4-4                               | Fehler beim Cache-Speichertest |

## Dell Fehlermeldungen (Optiplex)

**Diagnose-Fehlermeldungen** 

Dieser Abschnitt beschreibt die Fehlermedldungen und deren Lösungen.


| Fehlermeldung                                                                                                                                                                                                                                                                                                                                                                   | Hinweis                                                                                                                                                                                                                                                                                           |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Address mark not found (Adressmarkierung nicht gefunden)                                                                                                                                                                                                                                                                                                                        | Das BIOS hat einen fehlerhaften Festplattensektor gefunden oder konnte einen bestimmten Festplattensektor nicht finden.                                                                                                                                                                           |
| Alert! Previous attempts at booting this system have failed at checkpoint \[nnnn\] (Achtung!** **Frühere Versuche beim Starten des Systems sind am Prüfpunkt \[_nnnn_\] fehlgeschlagen). Um Hilfe zum Beheben des Problems zu bekommen, notieren Sie sich diesen Prüfpunkt und kontaktieren Sie den Technischen Support von Dell.                                               | Der Computer konnte die Startroutine dreimal hintereinander aufgrund desselben Fehlers nicht abschließen. Wenden Sie sich an Dell, und geben Sie dem Support-Techniker den Checkpoint-Code (_nnnn_) bekannt.                                                                                      |
| Alert! Jumper zum Aufheben der Sicherheitseinstellungen ist gesetzt.                                                                                                                                                                                                                                                                                                            | Der Jumper MFG\_MODE wurde gesetzt, und AMT-Verwaltungsfunktionen sind deaktiviert, bis Sie ihn entfernen.                                                                                                                                                                                        |
| Die Verbindung antwortet nicht.                                                                                                                                                                                                                                                                                                                                                 | Der Disketten- oder Festplattenlaufwerk-Controller kann keine Daten an das zugewiesene Laufwerk senden.                                                                                                                                                                                           |
| Ungültiger Befehl oder Dateiname                                                                                                                                                                                                                                                                                                                                                | Überprüfen Sie die Schreibweise des Befehls, die Position der Leerstellen und den angegebenen Zugriffspfad.                                                                                                                                                                                       |
| Falscher Fehlerkorrektur-Code (ECC-Code) beim Lesen                                                                                                                                                                                                                                                                                                                             | Der Diskettenlaufwerk- oder Festplat-tencontroller hat einen nicht korrigierbaren Lesefehler ermittelt.                                                                                                                                                                                           |
| Controller ist fehlerhaft                                                                                                                                                                                                                                                                                                                                                       | Die Festplatte oder der mit ihr verbundene Controller ist defekt.                                                                                                                                                                                                                                 |
| Datenfehler                                                                                                                                                                                                                                                                                                                                                                     | Das Disketten- bzw. Festplattenlauf-werk kann die Daten nicht lesen. Füh-ren Sie unter Windows® das Dienstprogramm chkdsk aus, um die Da-teistruktur des Disketten- bzw. Fest-plattenlaufwerks zu überprüfen. Führen Sie unter einem anderen Be-triebssystem das entsprechende Dienstprogramm aus.|
| Decreasing available memory (Weniger Speicher verfügbar)                                                                                                                                                                                                                                                                                                                        | Ein oder mehrere Speichermodule sind möglicherweise fehlerhaft oder nicht korrekt eingesetzt. Installieren Sie die Speichermodule erneut, oder tauschen Sie sie gegebenenfalls aus.                                                                                                               | 
| Fehler beim Suchen des Diskettenlaufwerks 0                                                                                                                                                                                                                                                                                                                                     | Möglicherweise hat sich ein Kabel gelöst oder die Informationen der Computerkonfiguration stimmen nicht mit der Hardwarekonfiguration überein.                                                                                                                                                    |
| Fehler beim Lesen der Diskette                                                                                                                                                                                                                                                                                                                                                  | Die Diskette ist möglicherweise defekt oder ein Kabel hat sich gelöst. Wenn die Laufwerkzugriffsanzeige aufleuchtet, verwenden Sie eine andere Diskette.                                                                                                                                          |
| Zurücksetzen des Diskettensubsystems fehlgeschlagen                                                                                                                                                                                                                                                                                                                             | Der Diskettenlaufwerk-Controller ist möglicherweise fehlerhaft.                                                                                                                                                                                                                                   |
| Laufwerk nicht bereit                                                                                                                                                                                                                                                                                                                                                           | Es befindet sich keine Diskette im Laufwerk. Legen Sie eine Diskette in das Laufwerk ein.                                                                                                                                                                                                         |
| Diskette ist schreibgeschützt.                                                                                                                                                                                                                                                                                                                                                  | Die Diskette ist schreibgeschützt. Verschieben Sie die Schreibschutzkerbe, sodass die Diskette beschrieben werden kann.                                                                                                                                                                           |
| Gate A20-Fehler                                                                                                                                                                                                                                                                                                                                                                 | Ein oder mehrere Speichermodule sind möglicherweise fehlerhaft oder nicht korrekt eingesetzt. Installieren Sie die Speichermodule erneut, oder tauschen Sie sie gegebenenfalls aus.                                                                                                               |
| Allgemeiner Fehler                                                                                                                                                                                                                                                                                                                                                              | Das Betriebssystem konnte den Befehl nicht ausführen. Auf diese Meldung folgen gewöhnlich detaillierte Informationen, beispielsweise **Printer out of paper** (Kein Papier im Drucker). Treffen Sie die entsprechenden Maßnahmen, um das Problem zu beheben.                                      |
| Hard-disk configuration error (Fehler bei der Festplattenkonfiguration)                                                                                                                                                                                                                                                                                                         | Die Festplatte konnte nicht initialisiert werden.                                                                                                                                                                                                                                                 |
| Hard-disk controller failure (Fehler des Festplatten-Controllers)                                                                                                                                                                                                                                                                                                               | Die Festplatte konnte nicht initialisiert werden.                                                                                                                                                                                                                                                 |
| Festplattenlaufwerkfehler                                                                                                                                                                                                                                                                                                                                                       | Die Festplatte konnte nicht initialisiert werden.                                                                                                                                                                                                                                                 |
| Fehler beim Lesen des Festplattenlaufwerks                                                                                                                                                                                                                                                                                                                                      | Die Festplatte konnte nicht initialisiert werden.                                                                                                                                                                                                                                                 |
| Invalid configuration information – please run SETUP program (Ungültige Konfigurationsinformationen – Führen Sie das Setup-Programm aus)                                                                                                                                                                                                                                        | Die Systemkonfigurationsdaten stimmen nicht mit der Hardware-Konfiguration überein.                                                                                                                                                                                                               |
| Invalid Memory configuration, please populate DIMM1 (Ungültige Speicherkonfiguration, setzen Sie ein Speichermodul in DIMM1 ein)                                                                                                                                                                                                                                                | Der DIMM1-Steckplatz erkennt ein Speichermodul nicht; das Modul sollte erneut eingesetzt werden.                                                                                                                                                                                                  |
| Tastaturfehler                                                                                                                                                                                                                                                                                                                                                                  | Eine Kabelverbindung ist unter Umständen lose, oder der Tastatur- oder Tastatur/Maus-Controller ist fehlerhaft.                                                                                                                                                                                   |
| Memory address line failure at _address_, read _value_ expecting _value_ (Speicher-Adressleitungsfehler an Speicheradresse x, Istwert/Sollwert)                                                                                                                                                                                                                                 | Ein Speichermodul ist möglicherweise fehlerhaft oder falsch eingesetzt. Installieren Sie die Speichermodule erneut, oder tauschen Sie sie gegebenenfalls aus.                                                                                                                                     |
| Memory allocation error (Fehler bei der Speicherzuweisung)                                                                                                                                                                                                                                                                                                                      | Das gerade gestartete Programm steht in Konflikt mit dem Betriebssystem, einem anderen Anwendungsprogramm oder einem Dienstprogramm.                                                                                                                                                              |
| Memory data line failure at _address_, read _value_ expecting _value_ (Speicher-Datenleitungsfehler an Adresse x, Istwert/Sollwert)                                                                                                                                                                                                                                             | Ein Speichermodul ist möglicherweise fehlerhaft oder falsch eingesetzt. Installieren Sie die Speichermodule erneut, oder tauschen Sie sie gegebenenfalls aus.                                                                                                                                     |
| Memory double word logic failure at _address_, read _value_ expecting _value_ (Doppelwort-Logikfehler an Speicheradresse x, Istwert/Sollwert)                                                                                                                                                                                                                                   | Ein Speichermodul ist möglicherweise fehlerhaft oder falsch eingesetzt. Installieren Sie die Speichermodule erneut, oder tauschen Sie sie gegebenenfalls aus.                                                                                                                                     |
| Memory odd/even logic failure at _address_, read _value_ expecting _value_ (Gerade/Ungerade-Logikfehler an Speicheradresse x, Istwert/Sollwert)                                                                                                                                                                                                                                 | Ein Speichermodul ist möglicherweise fehlerhaft oder falsch eingesetzt. Installieren Sie die Speichermodule erneut, oder tauschen Sie sie gegebenenfalls aus.                                                                                                                                     |
| Memory write/read failure at _address_, read _value_ expecting _value_ (Schreib-/Lesefehler an Speicheradresse x, Istwert/Sollwert)                                                                                                                                                                                                                                             | Ein Speichermodul ist möglicherweise fehlerhaft oder falsch eingesetzt. Installieren Sie die Speichermodule erneut, oder tauschen Sie sie gegebenenfalls aus.                                                                                                                                     |
| Memory size in CMOS invalid (CMOS-Speichergröße ungültig)                                                                                                                                                                                                                                                                                                                       | Die in den Computerkonfigurationsdaten abgelegte Speichergröße stimmt nicht mit dem tatsächlich im Computer installierten Speicher überein.                                                                                                                                                       |
| Speichertest durch Tastaturanschläge abgebrochen                                                                                                                                                                                                                                                                                                                                | Ein Tastendruck hat den Speichertest unterbrochen.                                                                                                                                                                                                                                                |
| No boot device available (Kein Startgerät verfügbar)                                                                                                                                                                                                                                                                                                                            | Der Computer kann das Disketten- oder Festplattenlaufwerk nicht finden.                                                                                                                                                                                                                           |
| Fehlender Startsektor auf Festplattenlaufwerk                                                                                                                                                                                                                                                                                                                                   | Die Konfigurationsinformationen des Computers im System-Setup sind möglicherweise nicht korrekt.                                                                                                                                                                                                  |
| Kein Zeitgeber-Tick-Interrupt                                                                                                                                                                                                                                                                                                                                                   | Ein Chip auf der Systemplatine ist möglicherweise defekt.                                                                                                                                                                                                                                         |
| Kein Systemdatenträger oder Datenträgerfehler                                                                                                                                                                                                                                                                                                                                   | Auf der Diskette in Laufwerk A ist kein startfähiges Betriebssystem installiert. Legen Sie eine Diskette mit einem startfähigen Betriebssystem ein, oder nehmen Sie die Diskette aus Laufwerk A und starten Sie den Computer neu.                                                                 |
| Keine Startdiskette                                                                                                                                                                                                                                                                                                                                                             | Das Betriebssystem versucht, von einer Diskette zu starten, auf der kein startfähiges Betriebssystem installiert ist. Legen Sie eine startfähige Diskette ein.                                                                                                                                    |
| Plug & Play Configuration Error (Plug-and-Play-Konfigurationsfehler)                                                                                                                                                                                                                                                                                                            | Der Computer hat ein Problem während des Versuchs festgestellt, eine oder mehrere Karten zu konfigurieren.                                                                                                                                                                                        |
| Lesefehler                                                                                                                                                                                                                                                                                                                                                                      | Das Betriebssystem kann vom Disketten- oder Festplattenlaufwerk nicht lesen, der Computer konnte einen bestimmten Sektor auf dem Laufwerk nicht finden, oder der angeforderte Sektor ist defekt.                                                                                                  |
| Angeforderter Sektor nicht gefunden                                                                                                                                                                                                                                                                                                                                             | Das Betriebssystem kann vom Disketten- oder Festplattenlaufwerk nicht lesen, der Computer konnte einen bestimmten Sektor auf dem Laufwerk nicht finden, oder der angeforderte Sektor ist defekt.                                                                                                  |
| Reset failed (Zurücksetzen fehlgeschlagen)                                                                                                                                                                                                                                                                                                                                      | Die Festplatte konnte nicht zurückgesetzt werden.                                                                                                                                                                                                                                                 |
| Sektor nicht gefunden                                                                                                                                                                                                                                                                                                                                                           | Das Betriebssystem kann einen Sektor auf dem Disketten- oder Festplattenlaufwerk nicht finden.                                                                                                                                                                                                    |
| Positionierungsfehler                                                                                                                                                                                                                                                                                                                                                           | Das Betriebssystem kann eine bestimmte Spur auf dem Disketten- oder Festplattenlaufwerk nicht finden.                                                                                                                                                                                             |
| Shutdownfehler                                                                                                                                                                                                                                                                                                                                                                  | Ein Chip auf der Systemplatine ist möglicherweise defekt.                                                                                                                                                                                                                                         |
| Uhrzeit funktioniert nicht                                                                                                                                                                                                                                                                                                                                                      | Der Akku ist möglicherweise leer.                                                                                                                                                                                                                                                                 |
| Time-of-day not set – please run the System Setup program (Uhrzeit nicht definiert; System-Setup-Programm aufrufen)                                                                                                                                                                                                                                                             | Die Uhrzeit- bzw. Datumsangaben, die im System-Setup-Programm gespeichert sind, stimmen nicht mit der Systemuhr überein.                                                                                                                                                                          |
| Fehler des Zeitgeber-Chipzählers 2                                                                                                                                                                                                                                                                                                                                              | Möglicherweise arbeitet ein Chip auf der Systemplatine nicht einwandfrei.                                                                                                                                                                                                                         |
| Unerwarteter Interrupt im geschützten Modus                                                                                                                                                                                                                                                                                                                                     | Der Tastatur-Controller ist möglicherweise defekt oder ein Speichermodul ist möglicherweise nicht richtig befestigt.                                                                                                                                                                              |
| WARNUNG: Das Datenträger-Überwachungssystem (Disk Monitoring System) von Dell hat festgestellt, dass das Laufwerk \[0/1\] am \[primären/sekundären\] EIDE-Controller außerhalb der normalen Angaben betrieben wird. Es wird empfohlen, sofort eine Sicherungskopie Ihrer Daten anzufertigen und die Festplatte auszutauschen. Rufen Sie dazu den Support-Desk oder Dell an.     | Beim erstmaligen Start hat das Laufwerk mögliche Fehlerzustände ermittelt. Sichern Sie sofort Ihre Daten, und ersetzen Sie die Festplatte, sobald der Computer den Startvorgang abgeschlossen hat.                                                                                                |
| Schreibfehler                                                                                                                                                                                                                                                                                                                                                                   | Das Betriebssystem kann nicht auf das Disketten- oder Festplattenlaufwerk schreiben.                                                                                                                                                                                                              |
| Schreibfehler auf ausgewähltem Laufwerk                                                                                                                                                                                                                                                                                                                                         | Das Betriebssystem kann nicht auf das Disketten- oder Festplattenlaufwerk schreiben.                                                                                                                                                                                                              |
| **_x_****:\\\\ is not accessible (Auf x:\\\\ kann nicht zugegriffen werden). Das Gerät ist nicht bereit**                                                                                                                                                                                                                                                                       | Das Diskettenlaufwerk kann den Da-tenträger nicht lesen. Legen Sie eine Diskette in das Laufwerk ein und ver-suchen Sie es erneut.                                                                                                                                                                |

