# Einleitung

Dieses Dokument beschreibt das Benutzeroberfläche (GUI) der MoWa Box, genannt MoWa Cockpit. Es werden alle Komponenten der GUI vorgestellt und die gängigsten Anwendungsbeispiele beschrieben.
Die MoWa  Box ist eine Appliance, welche die Anbindung zentraler Services realisiert. Durch Einbringen der Box in die lokale Netzwerkinfrastruktur des Partnerbetriebes und unter Verwendung des
vorhandenen Internetzu-gangs kann eine transparente sowie sichere Kommunikation zwischen den vorgesehenen lokalen Applikationen (z.B. DMS oder DMS-BB) und den benötigten zentralen Diensten
(z.B. Superservice Triage) hergestellt werden. Das MoWa Cockpit ist die Benutzeroberfläche (GUI) der MoWa PF Box. Mittels dieser GUI können sie Einstellungen an Ihrer MoWa PF vornehmen und
Angaben zu dessen Status erhalten.

