BITBUCKET_BRANCH="local"
BITBUCKET_BUILD_NUMBER:=$(shell date +"%s")
VERSION:="$(shell cat version)-${BITBUCKET_BRANCH}-${BITBUCKET_BUILD_NUMBER}"
# file
INPUT_FILE="documentation.md"
OUTPUT_FILE="result/documentation-${VERSION}.pdf"
WIN_DESKTOP_DIR="/mnt/c/Users/Stefan/Desktop/"

build:
	echo ${OUTPUT_FILE}
	@if [ ! -d result ]; then mkdir result; fi
	@docker run -v `pwd`:/data mw-ubuntulatex ${INPUT_FILE} -t pdf -o ${OUTPUT_FILE} --from markdown --template eisvogel --listings --number-sections -V lang=de 
# works only for smetzner
	@cp ${OUTPUT_FILE} ${WIN_DESKTOP_DIR}
